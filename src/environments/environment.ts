// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyBkCfk8SYwMlnpS1wC_IYSeYMr3aBVBmhE',
    authDomain: 'controle-douglas.firebaseapp.com',
    projectId: 'controle-douglas',
    storageBucket: 'controle-douglas.appspot.com',
    messagingSenderId: '79659778191',
    appId: '1:79659778191:web:2c0295cb8e560e6325f0fa',
    measurementId: 'G-0074NNKG0G'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
